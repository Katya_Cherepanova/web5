function calculyator()
{
    let c = document.getElementById("crime").value;
    let k = document.getElementById("kol").value;
    if((c.match(/^[0-9]*[.]?[0-9]+$/) === null) || (k.match(/^[0-9]*[.]?[0-9]+$/) === null)){
        document.getElementById("itog").innerHTML="Ошибка";
        alert("Некорректные входные данные(Запишите вещественное число через точку)");
    }
    else{
        let plata = c*k;
        document.getElementById("itog").innerHTML="Оплатить: " + plata;
    }
}

window.addEventListener("DOMContentLoaded", function (event)
{
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button");
    b.addEventListener("click", calculyator);
});
